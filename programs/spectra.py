import math as ma
import numpy as np
import statistics as stat
import matplotlib
import matplotlib.pyplot as plt
import scipy.optimize as optim

import lib


def plot_both_voltages():
    domain = list(range(8192))
    fig, axs = plt.subplots(1, 2, sharex=True)

    for file in ['spectrum_50V', 'spectrum_25V']:
        counts = lib.get_counts(file)
        for i in range(2):
            if '50' in file:
                label = '50V (recommended)'
            else:
                label = '25V'
            axs[i].plot(domain, counts, label=label)
        axs[0].set_xlabel('channel')
        axs[1].set_xlabel('channel')
        axs[0].set_ylabel('count')
        axs[1].semilogy()
        axs[0].set_ylim(0)

    plt.legend()
    plt.show()

    return None


def channel_to_energy(channel, a, b):
    return a + b*channel

def calibrate():
    channels = list(range(8192))
    plt.xlabel('channel')
    plt.ylabel('energy (MeV)')

    #First, Method A is done.
    known_channels = [5494, 5542, 5593]
    known_energies = [5.443, 5.486, 5.545]
    plt.scatter(known_channels, known_energies, color='red',
                label='known energies of Am-241 peaks')
    params = optim.curve_fit(channel_to_energy, known_channels, known_energies)
    a1 = params[0][0]
    b1 = params[0][1]
    image = [channel_to_energy(channel, a1, b1) for channel in channels]
    plt.plot(channels, image, label='fit for Am-241 peaks')

    #Now, Method B is done.
    known_channels = [1025, 2048, 3069, 4098, 5118]
    known_energies = [1., 2., 3., 4., 5.]
    plt.scatter(known_channels, known_energies, color='yellow',
                label='known energies using pulse generator')
    params = optim.curve_fit(channel_to_energy, known_channels, known_energies)
    a2 = params[0][0]
    b2 = params[0][1]
    image = [channel_to_energy(channel, a2, b2) for channel in channels]
    plt.plot(channels, image, label='fit for pulse generator')

    plt.legend()
    plt.show()

    return a1, a2, b1, b2


def comp_resolution(b1, b2):
    num_channels = 8.37
    print('The resolutions for Method A and Method B are '
          + f'{round(b1*num_channels, 5)} MeV '
          + f'and {round(b2*num_channels, 5)} MeV, respectively.')
    return None


def plot_attenuation(): #Make this a side-by-side plot.
    layers = [0, 2, 4, 6, 8]
    energies = [5.477, 4.723, 3.939, 3.081, 1.904]
    plt.plot(layers, energies, marker='o')
    plt.xlabel('number of layers')
    plt.ylabel('energy (MeV)')

    plt.show()

    return None


def plot_spectrum(a, b):
    domain = list(range(8192))
    domain = [channel_to_energy(x, a, b) for x in domain]
    counts = lib.get_counts('am_241_spectrum')

    plt.plot(domain, counts)
    plt.xlabel('energy (MeV)')
    plt.ylabel('count')
    #plt.semilogy()
    plt.show()


if __name__ == '__main__':
    matplotlib.rcParams.update({'font.size': 14})
    plot_both_voltages()
    b2, a2, b1, b2 = calibrate()
    comp_resolution(b1, b2)
    plot_attenuation()
    plot_spectrum(a2, b2)


'''
Activity of the source.
'''
if __name__ == '__main__':
    time = 106558.6 #In seconds.
    counts = 242617
    activity_measured = counts/time
    solid_angle = 2*np.arcsin((.35/2.)/.79)
    print(f'The solid angle is {round(solid_angle*180/(2*np.pi), 5)} degrees.')
    a = .35/2.
    d = .79
    efficiency = .5*(1 - d/ma.sqrt(a**2 + d**2))
    activity_absolute = activity_measured / efficiency
    print(f'The absolute activity is {activity_absolute} Bq.')

    initial_activity = 1.270 * 10**(-6) * 3.7 * 10**10 #In becquerels
    print(f'The intiail activity was {initial_activity} Bq.')
    time_since = 6547 * 24 * 60 * 60 #In seconds
    half_life = 432.2 * 365 * 24 * 60 * 60
    decay_exponent = -time_since*ma.log(2.)/half_life
    activity_predicted = initial_activity*ma.exp(decay_exponent)
    print(f'The predicted acitivty is {round(activity_predicted, 5)} Bq.')
